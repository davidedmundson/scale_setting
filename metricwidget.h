#ifndef ATHING
#define ATHING

#include <QPaintDevice>
#include <QWidget>

template <class P> class MetricWidget : public P
{
public:
  MetricWidget(QWidget *parent = 0) : P(parent) {}
protected:
  int metric(QPaintDevice::PaintDeviceMetric m) const
  {
    if (m == QPaintDevice::PdmDpiX || m == QPaintDevice::PdmDpiY) {
	  return 72 * 3;
      } else if (m == QPaintDevice::PdmDevicePixelRatio) {
	qDebug() << this << "ASDF";  
	return 4;
      }
      return QWidget::metric(m);
  }
};

#endif