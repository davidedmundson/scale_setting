/*
 * <one line to give the library's name and an idea of what it does.>
 * Copyright 2015  <copyright holder> <email>
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of
 * the License or (at your option) version 3 or any later version
 * accepted by the membership of KDE e.V. (or its successor approved
 * by the membership of KDE e.V.), which shall act as a proxy
 * defined in Section 14 of version 3 of the license.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * 
 */

#include "previewwidget.h"

#include <QDebug>
#include <QPainter>

#include "ui_stylepreview.h"


PreviewWidget::PreviewWidget(QWidget *parent):
    QLabel(parent),
    m_scale(1.0)
{
  m_internalPreview = new QWidget; // deliberately no parent, we don't want it to have a screen
  
  Ui::StylePreview ui;
  ui.setupUi(m_internalPreview );
  
  setScale(100);
}

PreviewWidget::~PreviewWidget()
{
    delete m_internalPreview;
}

void PreviewWidget::setScale(qreal scale)
{
    m_scale = scale / 100;
    
    int dpr = qRound(m_scale);
    
    QFont font;
    qreal oldPointSize = font.pointSize();
    font.setPixelSize(oldPointSize * 96.0 * (m_scale/ dpr)  / 72.0);
    m_internalPreview->setFont(font);
    m_internalPreview->adjustSize();

    updatePixmapCache();
}

void PreviewWidget::updatePixmapCache()
{
   int dpr = qRound(m_scale);
   QPixmap pixmap(m_internalPreview ->size() * dpr);
   pixmap.setDevicePixelRatio(dpr);
   QPainter p(&pixmap);
   m_internalPreview ->render(&p);

   //render back at whatever the native DPR of the KCM is
   pixmap.setDevicePixelRatio(devicePixelRatio());
      
   setPixmap(pixmap);
}


