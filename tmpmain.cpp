#include <QApplication>

#include <QWidget>

#include "ui_scaling.h"

class Scale : public QWidget
{
public:
  Scale();
  
};

Scale::Scale(): QWidget()
{
    Ui::Scaling ui;
    ui.setupUi(this);
    connect(ui.scaleSlider, &QSlider::valueChanged, ui.previewWidget, &PreviewWidget::setScale);
    connect(ui.scaleSlider, &QSlider::valueChanged, ui.scaleLabel, [=](qreal value) {
	ui.scaleLabel->setText(QString::number(value / 100.0));
    });

}

int main(int argc, char ** argv)
{
    QApplication app(argc, argv);
    
    Scale w;
    w.show();
    w.resize(600,400);
     
    return app.exec();
}